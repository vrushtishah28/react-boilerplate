import React from 'react';
import Board from 'react-trello';
import { Paper } from '@material-ui/core';
import data from './data.json';
import './trello.css';
function Trello() {
  return (
    <Paper elevation={3} className="py-5 px-2">
      <h1>Kanban Board</h1>
      <div className="trello-wrapper">
        <Board data={data} draggable editable editLaneTitle canAddLanes style={{ backgroundColor: 'transparent' }} />
      </div>
    </Paper>
  );
}

export default Trello;
