import React from 'react';
import TestRenderer from 'react-test-renderer';
import Trello from '../Trello';

describe('Component', () => {
  const testRenderer = TestRenderer.create(<Trello />);
  const testInstance = testRenderer.root;
  test('Component', () => {
    expect(testInstance.toJSON).toMatchSnapshot();
  });
});
