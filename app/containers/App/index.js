import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';
import './index.css';

import Layout from 'containers/Layout/index';
// import GlobalStyle from '../../global-styles';

export default function App() {
  return (
    <>
      <Helmet titleTemplate="Component Library" defaultTitle="Component Library">
        <meta name="description" content="A React.js Boilerplate application" />
      </Helmet>
      <div style={{ height: '70px', width: '100%' }} />
      <Switch>
        <Route path="/" component={Layout} />
      </Switch>
      {/* <GlobalStyle /> */}
    </>
  );
}
