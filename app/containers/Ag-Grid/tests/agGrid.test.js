import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AgGridThemes from '../AgGridThemes';
import AgGridScroll from '../AgGridScroll';
import AgGridAutoWidth from '../AgGridAutoWidth';
import AgGridPage from '../AgGridPage';

configure({ adapter: new Adapter() });
describe('AgGridThemes', () => {
  it('should be defined', () => {
    expect(AgGridThemes).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<AgGridThemes />);
    expect(tree).toMatchSnapshot();
  });
  it('onSelectChange', () => {
    const wrapper = shallow(<AgGridThemes />);
    // const Btn = wrapper.find('select');
    wrapper.find('select').simulate('change', { target: { value: 'hello' } });

    // const text = Btn.props().children;
    // expect(text).toEqual('Show Data');
  });
});
describe('AgGridScroll', () => {
  it('should be defined', () => {
    expect(AgGridScroll).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<AgGridScroll />);
    expect(tree).toMatchSnapshot();
  });
});
describe('AgGridAutoWidth', () => {
  it('should be defined', () => {
    expect(AgGridAutoWidth).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<AgGridAutoWidth />);
    expect(tree).toMatchSnapshot();
  });
});
describe('AgGridPage', () => {
  it('should be defined', () => {
    expect(AgGridPage).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<AgGridPage />);
    expect(tree).toMatchSnapshot();
  });
});
