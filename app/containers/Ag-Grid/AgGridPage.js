import React from 'react';
import CardList from '../../components/cards/CardList';
import { AgGridLibrary } from '../../components/Ag-Grid/constant';

function AgGridPage() {
  return (
    <>
      <section>
        <div className="container">
          <h1 className="text-center ">Ag-Grid Components</h1>
        </div>
      </section>
      <section>
        <div className="container">
          <CardList data={AgGridLibrary} className="cardlist" />
        </div>
      </section>
    </>
  );
}

export default AgGridPage;
