import React, { useState } from 'react';
import { Paper } from '@material-ui/core';
import Grid from '../../components/Ag-Grid/Grid';
import { theme } from '../../components/Ag-Grid/constant';

function AgGridThemes() {
  const [selectedTheme, setSelectedTheme] = useState(theme[0]);
  return (
    <>
      <section>
        <div className="container">
          <h1>Ag-Grid Themes</h1>

          <select className="form-control" value={selectedTheme} onChange={e => setSelectedTheme(e.target.value)}>
            {theme.map(item => (
              <option value={item}>{item}</option>
            ))}
          </select>
        </div>
      </section>
      <section>
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-6 m-auto">
              <h2>Syntax</h2>
              <Paper elevation={3} className="syntax-highlighter">
                <p className="code-container">
                  {`import "ag-grid-community/dist/styles/${selectedTheme}.css";     
        <div className='${selectedTheme}'>
                <AgGridReact
                columnDefs={gridData.columnDefs}
                rowData={gridData.rowData}/>
        </div>`}
                </p>
              </Paper>
            </div>
            <div className="col-md-6">
              <Grid theme={selectedTheme} height="300px" width="100%" autowidth />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default AgGridThemes;
