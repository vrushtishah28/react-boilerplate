## Component Name

Ag-Grid Component

## Getting Started

These code will help you in implementing ag-grid component easily.

### Prerequisites

Things you need to install before starting with ag-grid:

```
npm install --save ag-grid-community ag-grid-react

```

### How to Use

A step by step series of how you can implement Ag-grid in react:

Firstly start by importing libraries:

```
import { AgGridReact } from 'ag-grid-react';

``` 
If you want to implement themes in ag-grid then you need to import following statements and select the theme you like:

```

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-community/dist/styles/ag-theme-balham-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-blue.css';
import 'ag-grid-community/dist/styles/ag-theme-bootstrap.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-fresh.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';

```

## For using Ag-Grid components you'll have to implement the following code:

```

<AgGridReact columnDefs={data.columnDefs} rowData={data.rowData} />

```
### For more information you can go through the following link:


* [Reference Ag-grid Link](https://www.ag-grid.com/react-grid/) 


