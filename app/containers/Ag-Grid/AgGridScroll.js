import React from 'react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-blue.css';
import Grid from '../../components/Ag-Grid/Grid';
function AgGridScroll() {
  return (
    <>
      <div
        style={{
          margin: 'auto',
          maxWidth: '80%',
          height: 400,
        }}
      >
        <h1>Ag Grid With ScrollBar</h1>
        <Grid width="100%" theme="ag-theme-blue" />
        <p>We are using Ag Grid With ScrollBar.Where you will get Horizontal and Vertical Scroll Bars.</p>
      </div>
    </>
  );
}
export default AgGridScroll;
