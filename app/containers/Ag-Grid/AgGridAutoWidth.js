import React from 'react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-blue.css';
import Grid from '../../components/Ag-Grid/Grid';

function AgGridAutoWidth() {
  return (
    <>
      <div
        style={{
          margin: 'auto',
          maxWidth: '80%',
          height: 400,
        }}
      >
        <h1>Ag Grid Without ScrollBar[Auto Resizing]</h1>
        <Grid width="100%" theme="ag-theme-blue" autowidth />
        <p>We are using Ag Grid Without ScrollBar.Where on minimizing the browser it will fit by itself within the browser.And on refreshing auto height width will be set as it was before.</p>
      </div>
    </>
  );
}
export default AgGridAutoWidth;
