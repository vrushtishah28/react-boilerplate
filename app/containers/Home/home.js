import React from 'react';
import CardList from '../../components/cards/CardList';
import { ComponentMainPage } from './constant';

function home() {
  return (
    <section>
      <div className="container">
        <h1 typography-material-ui>Components</h1>
        <CardList className="cardlist" data={ComponentMainPage} />
      </div>
    </section>
  );
}

export default home;
