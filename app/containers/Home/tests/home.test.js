import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from '../home';

configure({ adapter: new Adapter() });

describe('Home', () => {
  it('should be defined', () => {
    expect(Home).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Home />);
    expect(tree).toMatchSnapshot();
  });
});
