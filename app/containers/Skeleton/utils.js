export const darkThemeSkeleton = {
  color: '#515151',
  highlightColor: '#444',
  boxBackground: '#424242',
};
export const lightThemeSkeleton = {
  color: '#eee',
  highlightColor: '#f5f5f5',
  boxBackground: '#d3d3d3',
};
