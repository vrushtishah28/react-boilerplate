## Component Name

Skeleton Component

## Getting Started

These code will help you in implementing skeleton component easily.

### Prerequisites

Things you need to install before starting with skeleton:

```
npm install react-loading-skeleton

```

### How to Use

A step by step series of how you can implement Skeleton in react:

Firstly start by importing libraries:

```
import Skeleton from 'react-loading-skeleton';

```

## For using Skeleton components you can go through the following code:

For three lines of skeleton you need to write this:

```
 <Skeleton count={2} />

```

For giving height width to skeleton you can give height width explicitly:

```

<Skeleton width={100} height={100} />

```

The above one will give a box of 100\*100

### For more information you can go through the following link:

- [Reference Skeleton Link](https://github.com/dvtng/react-loading-skeleton)
