import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import './blog.css';
import PropTypes from 'prop-types';
import SkeletonComponent from './Skeleton';
import MainSkeletonData from './MainSkeletonData';
import Button from '../../components/Button/index';
import { darkThemeSkeleton, lightThemeSkeleton } from './utils';
function SkeletonPage({ theme }) {
  const [selectedTheme] = useState(theme === 'dark' ? darkThemeSkeleton : lightThemeSkeleton);
  const [showSkeleton, setShowSkeleton] = useState(true);
  const onClickHandler = () => {
    setShowSkeleton(!showSkeleton);
  };

  const name = showSkeleton ? 'Show Data' : 'Show Skeleton';
  return (
    <>
      <div className="skeleton-wrapper">
        <Helmet>
          <meta charset="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <meta name="description" content="" />
          <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors" />
          <meta name="generator" content="Jekyll v3.8.6" />
          <title>Blog Template · Bootstrap</title>

          <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/blog/" />

          <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous" />

          <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180" />
          <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png" />
          <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png" />
          <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json" />
          <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c" />
          <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico" />
          <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml" />
          <meta name="theme-color" content="#563d7c" />

          <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet" />
        </Helmet>
        <div className="text-center">
          <Button className="btn btn-primary" onClick={onClickHandler}>
            {name}
          </Button>
        </div>

        {showSkeleton ? <SkeletonComponent selectedTheme={selectedTheme} /> : <MainSkeletonData selectedTheme={selectedTheme} />}
      </div>
    </>
  );
}
SkeletonPage.propTypes = {
  theme: PropTypes.string,
};
SkeletonPage.defaultProps = {
  theme: '',
};
export default SkeletonPage;
