import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SkeletonPage from '../SkeletonPage';
import Skeleton from '../Skeleton';
import MainSkeletonData from '../MainSkeletonData';

configure({ adapter: new Adapter() });

describe('SkeletonPage', () => {
  it('should render correctly', () => {
    const tree = shallow(<SkeletonPage />);
    expect(tree).toMatchSnapshot();
  });
  it('onbuttonClick', () => {
    const wrapper = shallow(<SkeletonPage />);
    const Btn = wrapper.find('Button');
    Btn.simulate('click');
    const text = Btn.props().children;
    expect(text).toEqual('Show Data');
  });
});
describe('Skeleton', () => {
  it('should render correctly', () => {
    const tree = shallow(<Skeleton />);
    expect(tree).toMatchSnapshot();
  });
});
describe('Skeleton', () => {
  it('should render correctly', () => {
    const tree = shallow(<MainSkeletonData />);
    expect(tree).toMatchSnapshot();
  });
});
