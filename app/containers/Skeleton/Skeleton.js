import React from 'react';
import { Helmet } from 'react-helmet';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import PropTypes from 'prop-types';
import './blog.css';
function SkeletonComponent({ selectedTheme }) {
  return (
    <SkeletonTheme color={selectedTheme.color} highlightColor={selectedTheme.highlightColor}>
      <div className="skeleton-wrapper">
        <Helmet>
          <meta charset="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <meta name="description" content="" />
          <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors" />
          <meta name="generator" content="Jekyll v3.8.6" />
          <title>Blog Template · Bootstrap</title>

          <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/blog/" />

          <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous" />

          <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180" />
          <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png" />
          <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png" />
          <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json" />
          <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c" />
          <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico" />
          <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml" />
          <meta name="theme-color" content="#563d7c" />
        </Helmet>

        <div className="container">
          <header className="blog-header py-3" style={{ borderBottom: `1px solid ${selectedTheme.boxBackground}` }}>
            <div className="row flex-nowrap justify-content-between align-items-center">
              <div className="col-4 pt-1">
                <a className="text-muted" href="/subscribe">
                  Subscribe
                </a>
              </div>
              <div className="col-4 text-center">
                <h1 className="blog-header-logo text-center text-muted mb-0 mt-0" href="/subscribe">
                  Large
                </h1>
              </div>
              <div className="col-4 d-flex justify-content-end align-items-center">
                <a className="text-muted" href="/subscribe" aria-label="Search">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    className="mx-3"
                    role="img"
                    viewBox="0 0 24 24"
                    focusable="false"
                  >
                    <title>Search</title>
                    <circle cx="10.5" cy="10.5" r="7.5" />
                    <path d="M21 21l-5.2-5.2" />
                  </svg>
                </a>
                <a className="btn btn-sm btn-outline-secondary" href="/subscribe">
                  Sign up
                </a>
              </div>
            </div>
          </header>

          <div className="nav-scroller py-1 mb-2">
            <nav className="nav d-flex justify-content-between">
              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />

              <Skeleton width={70} />
            </nav>
          </div>

          <div className="jumbotron p-4 p-md-5 text-white rounded " style={{ backgroundColor: selectedTheme.boxBackground }}>
            <div className="col-md-6 px-0">
              <h1 className="display-4 font-italic">
                <Skeleton />
              </h1>
              <p className="lead my-3">
                <Skeleton count={3} />
              </p>
              <p className="lead mb-0">
                <a href="/subscribe" className="text-white font-weight-bold">
                  <Skeleton width={200} />
                </a>
              </p>
            </div>
          </div>

          <div className="row mb-2">
            <div className="col-md-6">
              <div className="row no-gutters rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style={{ border: `2px solid ${selectedTheme.boxBackground}` }}>
                <div className="col p-4 d-flex flex-column position-static">
                  <strong className="d-inline-block mb-2 text-primary">
                    <Skeleton width={70} />
                  </strong>
                  <h3 className="mb-0">
                    <Skeleton />
                  </h3>
                  <div className="mb-1 text-muted">
                    <Skeleton width={70} />
                  </div>
                  <p className="card-text mb-auto">
                    <Skeleton count={3} />
                  </p>
                  <a href="/subscribe" className="stretched-link">
                    <Skeleton width={100} />
                  </a>
                </div>
                <div className="col-auto d-none d-lg-block ">
                  <div style={{ position: 'relative', top: '-5px' }}>
                    <Skeleton height={250} width={200} />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="row no-gutters rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative" style={{ border: `2px solid ${selectedTheme.boxBackground}` }}>
                <div className="col p-4 d-flex flex-column position-static">
                  <strong className="d-inline-block mb-2 text-success">
                    <Skeleton width={70} />
                  </strong>
                  <h3 className="mb-0">
                    <Skeleton />
                  </h3>
                  <div className="mb-1 text-muted">
                    <Skeleton width={70} />
                  </div>
                  <p className="mb-auto">
                    <Skeleton count={3} />
                  </p>
                  <a href="/subscribe" className="stretched-link">
                    <Skeleton width={100} />
                  </a>
                </div>
                <div className="col-auto d-none d-lg-block">
                  <div style={{ position: 'relative', top: '-5px' }}>
                    <Skeleton height={250} width={200} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <main role="main" className="container">
          <div className="row">
            <div className="col-md-8 blog-main">
              <h3 className="pb-4 mb-4 font-italic" style={{ borderBottom: `2px solid ${selectedTheme.boxBackground}` }}>
                <Skeleton width={250} />
              </h3>

              <div className="blog-post">
                <h2 className="blog-post-title">
                  <Skeleton width={350} />
                </h2>
                <p className="blog-post-meta">
                  <Skeleton width={250} />
                </p>

                <p>
                  <Skeleton count={2} />
                </p>
                <hr />
                <p>
                  <Skeleton count={3} />
                </p>
                <blockquote>
                  <p>
                    <Skeleton count={2} />
                  </p>
                </blockquote>
                <p>
                  <Skeleton count={2} />
                </p>
                <h2>
                  <Skeleton width={150} />
                </h2>
                <p>
                  <Skeleton count={3} />
                </p>
                <h3>
                  {' '}
                  <Skeleton width={200} />
                </h3>
                <p>
                  <Skeleton />
                </p>
                <pre>
                  <code>
                    {' '}
                    <Skeleton width={150} />
                  </code>
                </pre>
                <p>
                  <Skeleton count={2} />
                </p>
                <h3>
                  {' '}
                  <Skeleton width={150} />
                </h3>
                <p>
                  <Skeleton count={4} />
                </p>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={200} />
                </div>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={200} />
                </div>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={200} />
                </div>
                <p>
                  <Skeleton />
                </p>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={300} />
                </div>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={300} />
                </div>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={300} />
                </div>
                <p>
                  <Skeleton />
                </p>
              </div>

              <div className="blog-post">
                <h2 className="blog-post-title">
                  {' '}
                  <Skeleton width={350} />
                </h2>
                <p className="blog-post-meta">
                  <Skeleton width={250} />
                </p>

                <p>
                  <Skeleton count={3} />
                </p>
                <blockquote>
                  <p>
                    <Skeleton count={2} />
                  </p>
                </blockquote>
                <p>
                  <Skeleton count={2} />
                </p>
                <p>
                  <Skeleton count={3} />
                </p>
              </div>

              <div className="blog-post">
                <h2 className="blog-post-title">
                  {' '}
                  <Skeleton width={250} />
                </h2>
                <p className="blog-post-meta">
                  <Skeleton width={200} />
                </p>

                <p>
                  <Skeleton count={4} />
                </p>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={300} />
                </div>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={300} />
                </div>
                <div className="d-flex mw-100">
                  <div className="mx-1">
                    <Skeleton height={10} width={10} circle />
                  </div>

                  <Skeleton width={300} />
                </div>
                <p>
                  <Skeleton count={2} />
                </p>
                <p>
                  <Skeleton />
                </p>
              </div>

              <nav className="blog-pagination">
                <div className="mr-2 d-inline-block">
                  <Skeleton width={100} />
                </div>
                <Skeleton width={100} />
              </nav>
            </div>

            <aside className="col-md-4 blog-sidebar">
              <div className="p-4 mb-3  rounded" style={{ backgroundColor: selectedTheme.boxBackground }}>
                <h4 className="font-italic">
                  {' '}
                  <Skeleton width={100} />
                </h4>
                <p className="mb-0">
                  <Skeleton count={4} />
                </p>
              </div>

              <div className="p-4">
                <h4 className="font-italic">
                  {' '}
                  <Skeleton width={130} />
                </h4>
                <div className="d-flex flex-column">
                  <Skeleton width={100} />

                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                  <Skeleton width={100} />
                </div>
              </div>

              <div className="p-4">
                <h4 className="font-italic">
                  {' '}
                  <Skeleton width={130} />
                </h4>
                <div className="d-flex flex-column">
                  <Skeleton width={100} />

                  <Skeleton width={100} />

                  <Skeleton width={100} />
                </div>
              </div>
            </aside>
          </div>
        </main>

        <div className="blog-footer" style={{ backgroundColor: selectedTheme.boxBackground }}>
          <p>
            Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.
          </p>
          <p>
            <a href="/subscribe">Back to top</a>
          </p>
        </div>
      </div>
    </SkeletonTheme>
  );
}
SkeletonComponent.propTypes = {
  selectedTheme: PropTypes.string,
};
SkeletonComponent.defaultProps = {
  selectedTheme: '',
};
export default SkeletonComponent;
