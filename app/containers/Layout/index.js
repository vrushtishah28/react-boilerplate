import React, { Fragment, useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Switch, Route, Link } from 'react-router-dom';
import SkeletonPage from 'containers/Skeleton/SkeletonPage';
import AgGridPage from 'containers/Ag-Grid/AgGridPage';
import home from 'containers/Home/home';
import AgGridThemes from 'containers/Ag-Grid/AgGridThemes';
import AgGridScroll from 'containers/Ag-Grid/AgGridScroll';
import AgGridAutoWidth from 'containers/Ag-Grid/AgGridAutoWidth';
import ReactToastify from 'containers/Toast/ReactToastify';
import CardLoader from 'containers/Loader/CardLoader';
import Calendar from 'containers/Calendar/Calendar';
import Trello from 'containers/Trello/Trello';
import FlashAutoIcon from '@material-ui/icons/FlashAuto';
import FlashOffIcon from '@material-ui/icons/FlashOff';
import { Tooltip } from '@material-ui/core';
import { ListObject } from './utils';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    // display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  list: {
    paddingLeft: '23px',
  },
  header: {
    color: 'white',
  },
}));

export default function MiniDrawer() {
  const [themeMode, setThemeMode] = useState('dark');
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const darkTheme = createMuiTheme({
    palette: {
      type: 'dark',
      primary: {
        main: '#007BFF',
      },
    },
  });
  const changeTheme = () => {
    if (themeMode === 'dark') {
      setThemeMode('light');
    } else {
      setThemeMode('dark');
    }
  };
  return (
    <div className={classes.root}>
      <ThemeProvider theme={themeMode === 'dark' ? darkTheme : theme}>
        <CssBaseline />
        <AppBar
          position="fixed"
          style={{ backgroundColor: themeMode === 'dark' ? '#333333' : null }}
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <MenuIcon />
            </IconButton>
            <Link to="/" className={classes.header}>
              <Typography variant="h6" noWrap>
                Component Library
              </Typography>
            </Link>
            <IconButton onClick={() => changeTheme()} className="ml-auto">
              {themeMode === 'dark' ? (
                <Tooltip title="Go to Light Theme" aria-label="dark">
                  <FlashOffIcon />
                </Tooltip>
              ) : (
                <Tooltip title="Go to Dark Theme" aria-label="light">
                  <FlashAutoIcon />
                </Tooltip>
              )}
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={handleDrawerClose}>{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}</IconButton>
          </div>
          <Divider />
          <List>
            {ListObject.map((item, index) => (
              <Fragment>
                <ListItem className={classes.list} button key={item.name} component={Link} to={item.link}>
                  <ListItemIcon>{item.icon}</ListItemIcon>
                  <ListItemText primary={item.name} />
                </ListItem>
                {index === 0 ? <Divider /> : null}
              </Fragment>
            ))}
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar}>
            <Switch>
              <Route path="/Trello" component={Trello} />
              <Route path="/Calendar" component={Calendar} />
              <Route path="/Skeleton" component={() => <SkeletonPage theme={themeMode} />} />
              <Route path="/AgGrid-scroll" component={AgGridScroll} />
              <Route path="/AgGrid-noscroll" component={AgGridAutoWidth} />
              <Route path="/AgGrid-themes" component={AgGridThemes} />
              <Route path="/AgGridPage" component={AgGridPage} />
              <Route path="/Toast" component={ReactToastify} />
              <Route path="/CardLoader" component={CardLoader} />
              <Route path="/" component={home} />
            </Switch>
          </div>
        </main>
      </ThemeProvider>
    </div>
  );
}
