import GridOnIcon from '@material-ui/icons/GridOn';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import MessageIcon from '@material-ui/icons/Message';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import HomeIcon from '@material-ui/icons/Home';
import AllInboxIcon from '@material-ui/icons/AllInbox';
import React from 'react';

export const ListObject = [
  {
    name: 'Home',
    icon: <HomeIcon />,
    link: '/',
  },
  {
    name: 'Ag-grid',
    icon: <GridOnIcon />,
    link: '/AgGridPage',
  },
  {
    name: 'React Toastify',
    icon: <ChatBubbleIcon />,
    link: '/Toast',
  },
  {
    name: 'Loading',
    icon: <RotateLeftIcon />,
    link: '/CardLoader',
  },
  {
    name: 'Skeleton',
    icon: <MessageIcon />,
    link: '/Skeleton',
  },
  {
    name: 'Calendar',
    icon: <CalendarTodayIcon />,
    link: '/Calendar',
  },
  {
    name: 'React Trello',
    icon: <AllInboxIcon />,
    link: '/Trello',
  },
];
