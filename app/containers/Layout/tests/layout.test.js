import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Layout from '../index';
configure({ adapter: new Adapter() });

describe('Layout', () => {
  it('should be defined', () => {
    expect(Layout).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Layout />);
    expect(tree).toMatchSnapshot();
  });
});
