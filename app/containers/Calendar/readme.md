## Component Name

Calendar Component

## Getting Started

These code will help you in implementing Calendar component easily.

### Prerequisites

Dependencies you need to install before starting with Calendar:

```
npm install @devexpress/dx-react-core
npm install @devexpress/dx-react-scheduler
npm install @devexpress/dx-react-scheduler-material-ui
npm install @material-ui/core
npm install @material-ui/icons

```

### How to Use

A step by step series of how you can implement Ag-grid in react:

Firstly start by importing libraries:

```
import Paper from '@material-ui/core/Paper';
import { ViewState, EditingState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  Toolbar,
  MonthView,
  WeekView,
  ViewSwitcher,
  Appointments,
  AppointmentTooltip,
  AppointmentForm,
  DragDropProvider,
  EditRecurrenceMenu,
  AllDayPanel,
} from '@devexpress/dx-react-scheduler-material-ui';
import { connectProps } from '@devexpress/dx-react-core';
import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import LocationOn from '@material-ui/icons/LocationOn';
import Notes from '@material-ui/icons/Notes';
import Close from '@material-ui/icons/Close';
import CalendarToday from '@material-ui/icons/CalendarToday';
import Create from '@material-ui/icons/Create';
import { appointments } from './demo-data/appointments';

```

## For using Calendar components you'll have to implement the following code:

```

<Paper>
        <Scheduler data={data} height={660}>
          <ViewState
            currentDate={currentDate}
            onCurrentDateChange={this.currentDateChange}
          />
          <EditingState onCommitChanges={this.onCommitChanges} />
          <EditRecurrenceMenu />
          <WeekView startDayHour={9} endDayHour={19} />
          <MonthView />
          <Toolbar />
          <ViewSwitcher />
          <DateNavigator />
          <TodayButton />
          <Appointments />

          <AppointmentTooltip showOpenButton showCloseButton />
          <AppointmentForm />
          <DragDropProvider allowDrag={allowDrag} />
        </Scheduler>
      </Paper>

```

### For more information you can go through the following link:

- [Reference Calendar Link](https://devexpress.github.io/devextreme-reactive/react/scheduler/demos/featured/overview/)
