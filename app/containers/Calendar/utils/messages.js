/**
 * DashboardPage Messages
 *
 * This contains all the text for the DashboardPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  deleteAppointmentTitle: {
    id: 'componentLibrary.Calendar.deleteAppointmentForm.title',
    defaultMessage: 'Delete Appointment',
  },
  deleteAppointmentDesc: {
    id: 'componentLibrary.Calendar.deleteAppointmentForm.desc',
    defaultMessage: 'Are you sure you want to delete the appointment?',
  },
});
