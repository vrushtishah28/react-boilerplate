/* eslint-disable no-undef */
import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';
import Fab from '@material-ui/core/Fab';
import AppointmentFormContainer from '../AppointmentFormContainer';
import Calendar from '../Calendar';
configure({ adapter: new Adapter() });

describe('<AppointmentFormContainer />', () => {
  let shallow1;
  beforeAll(() => {
    shallow1 = createShallow();
  });

  it('should work', () => {
    const wrapper = shallow1(<AppointmentFormContainer />);
    expect(wrapper).toMatchSnapshot();
  });
});
describe('<Calendar />', () => {
  it('should work', () => {
    const wrapper = shallow(<Calendar />);
    expect(wrapper).toMatchSnapshot();
  });
  it('should simulate', () => {
    const wrapper = shallow(<Calendar />);
    const Btn = wrapper.find(<Fab />);
    Btn.simulate('click');
  });
});
