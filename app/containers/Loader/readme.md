## Component Name

Loader Component

## Getting Started

These code will help you in implementing Loader component easily.

### How to Use

A step by step series of how you can implement Loader in react:

Firstly maintain a state for loader:

```

const [loaderCards, setLoaderCards] = useState(true);

```

Second, based on condition render component:

```

if(loaderCards)
return <LoadingComponent />
else
return <Page />

```

### For more information you can go through the following link:

- [Reference Loader Link](https://getbootstrap.com/docs/4.4/components/spinners/)
