import React, { useState, Fragment } from 'react';
import Card from '../../components/Loader/Card';

function CardLoader() {
  const [loaderCards, setLoaderCards] = useState(false);
  const [disableButton, setDisableButton] = useState(false);

  const onClickButtonHandler = () => {
    setDisableButton(true);
    setTimeout(() => {
      setDisableButton(false);
    }, 2000);
  };
  return (
    <>
      <section>
        <div className="container">
          <h2 className="text-center mb-5">Card Loader</h2>
          <div className="ml-3 mb-3">
            <button
              id="btn-loader"
              type="button"
              className="btn btn-primary px-2 py-2 mr-3"
              onClick={() => {
                setLoaderCards(true);
              }}
            >
              Show Loader
            </button>
            <button
              id="btn-cancel"
              type="button"
              className="btn btn-danger px-2 py-2"
              onClick={() => {
                setLoaderCards(false);
              }}
            >
              Cancel
            </button>
          </div>
          <div className="row">
            {[1, 2, 3].map(() => (
              <div className="col-md-4 ">
                <div className="ml-3 mb-3" />
                <div
                  className="mb-4"
                  style={{
                    width: '100%',
                    display: 'block',
                  }}
                >
                  <div
                    className="d-flex justify-content-center align-items-center"
                    style={{
                      height: '450px',
                      width: '100%',
                    }}
                  >
                    <Card status={loaderCards} />
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>

      <section>
        <div className="container text-center mb-5">
          <h2 className="mb-5">Button Loader</h2>

          <button
            id="btn-loading"
            className={`btn ${disableButton ? 'btn-secondary' : 'btn-primary'} px-5 py-3 position-relative`}
            type="button"
            disabled={disableButton}
            onClick={onClickButtonHandler}
          >
            {disableButton ? (
              <Fragment>
                <span>Loading</span>
                <span className="spinner-border spinner-border-sm position-absolute" style={{ right: '10%', top: '35%' }} role="status" aria-hidden="true" />
              </Fragment>
            ) : (
              <span>Submit</span>
            )}
          </button>
        </div>
      </section>
    </>
  );
}

export default CardLoader;
