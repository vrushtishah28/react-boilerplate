import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Loader from '../CardLoader';
configure({ adapter: new Adapter() });

describe('Loader', () => {
  it('should be defined', () => {
    expect(Loader).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Loader />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly', () => {
    const wrapper = shallow(<Loader />);
    jest.useFakeTimers();
    const Btn = wrapper.find('#btn-loading');
    Btn.simulate('click');
    const text = Btn.props().children;
    expect(text).toEqual(<span>Submit</span>);
    jest.advanceTimersByTime(2000);
  });
  it('should render correctly', () => {
    const wrapper = shallow(<Loader />);
    const Btn = wrapper.find('#btn-loader');
    Btn.simulate('click');
    const text = Btn.props().children;
    expect(text).toEqual('Show Loader');
  });
  it('should render correctly', () => {
    const wrapper = shallow(<Loader />);
    const Btn = wrapper.find('#btn-cancel');
    Btn.simulate('click');
    const text = Btn.props().children;
    expect(text).toEqual('Cancel');
  });
});
