import React, { useState } from 'react';
import { toast, Slide, Flip, Bounce, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblem';
import './ReactToastify.css';
import { Paper } from '@material-ui/core';
import { toastDirectionData, toastType } from '../../components/Toast/constant';
import Message from './Message';

function ReactToastify() {
  const [direction, setDirection] = useState(toastDirectionData[0].value);
  const [toastTypeFlag, setToastTypeFlag] = useState(toastType[0].type);
  const [toastTransitions, setToastTransitions] = useState(0);
  const [autoClose] = useState(false);
  const [hideProgressBar] = useState(true);

  const transitionMap = {
    0: Slide,
    1: Flip,
    2: Bounce,
    3: Zoom,
  };
  const color = {
    success: 'toast-success',
    error: 'toast-error',
    info: 'toast-info',
    warning: 'toast-warning',
    default: 'toast-default',
  };
  const showToast = type => {
    const callingFunction = {
      success: toast.success,
      error: toast.error,
      info: toast.info,
      warning: toast.warning,
      default: toast,
    };

    const symbol = {
      success: <CheckCircleOutlineOutlinedIcon style={{ fill: 'green' }} />,
      error: <ErrorOutlineIcon style={{ fill: 'red' }} />,
      info: <InfoIcon style={{ fill: 'blue' }} />,
      warning: <ReportProblemOutlinedIcon style={{ fill: 'yellow' }} />,
      default: null,
    };

    callingFunction[type](<Message symbol={symbol[type]} msg="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ut enim sit amet erat posuere sollicitudin pretium quis velit. " />, {
      position: direction,
      autoClose: !autoClose,
      hideProgressBar,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      className: `text-dark bg-light ${color[toastTypeFlag]}`,
      transition: transitionMap[toastTransitions],
    });
  };
  toast.configure();

  return (
    <div className="toastify-wrapper container">
      <div className="row">
        <div className="col-md-8">
          <h1 className="text-center mb-5">React Toastify</h1>
          <div className="row">
            <div className="col-md-6">
              <h5 className="mb-3">Direction</h5>
              <div className="radio-group-direction">
                {toastDirectionData.map(item => {
                  const { name, value } = item;
                  return (
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        value={value}
                        type="radio"
                        name={name}
                        id={value}
                        checked={direction === value}
                        onChange={e => {
                          setDirection(e.target.value);
                        }}
                      />
                      <label className="form-check-label" htmlFor={value}>
                        {value}
                      </label>
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="col-md-6">
              <h5 className="mb-3">Type</h5>
              <div className="radio-group-type">
                {toastType.map(item => {
                  const { type, name } = item;
                  return (
                    <div className="form-check">
                      <input
                        value={type}
                        className="form-check-input"
                        type="radio"
                        name={name}
                        id={type}
                        checked={toastTypeFlag === type}
                        onChange={e => {
                          setToastTypeFlag(e.target.value);
                        }}
                      />
                      <label className="form-check-label" htmlFor={type}>
                        {type}
                      </label>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>

          <div className="row mt-5">
            <div className="col-md-8">
              <h5 className="mb-3">Transition Effects</h5>
              <div className="d-flex justify-content-between">
                <div className="form-check">
                  <label className="form-check-label" htmlFor="Slide">
                    <input
                      className="form-check-input"
                      value="0"
                      type="radio"
                      name="transition-toast"
                      id="Slide"
                      checked={toastTransitions === 0}
                      onChange={() => {
                        setToastTransitions(0);
                      }}
                    />
                    Slide
                  </label>
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="Flip">
                    <input
                      className="form-check-input"
                      value="1"
                      type="radio"
                      name="transition-toast"
                      id="Flip"
                      checked={toastTransitions === 1}
                      onChange={() => {
                        setToastTransitions(1);
                      }}
                    />
                    Flip
                  </label>
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="Bounce">
                    <input
                      className="form-check-input"
                      value="2"
                      type="radio"
                      name="transition-toast"
                      id="Bounce"
                      checked={toastTransitions === 2}
                      onChange={() => {
                        setToastTransitions(2);
                      }}
                    />
                    Bounce
                  </label>
                </div>
                <div className="form-check">
                  <label className="form-check-label" htmlFor="Zoom">
                    <input
                      className="form-check-input"
                      value="3"
                      type="radio"
                      name="transition-toast"
                      id="Zoom"
                      checked={toastTransitions === 3}
                      onChange={() => {
                        setToastTransitions(3);
                      }}
                    />
                    Zoom
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 sticky-top">
          <section>
            <h2 className="mb-4 mt-3 text-center">Toast Container</h2>
            <Paper elevation={3} className="syntax-highlighter">
              <p className="code-container">
                {`const showToast = () => {
                 toast.${toastTypeFlag}(" Wow so easy!", {
                 position: ${direction},
                 autoClose: false,
                 hideProgressBar: true,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true,
                  });
                 }
                  <button className="btn-primary"
                  onClick={() => showToast()}>
                          Show Toast
                 </button>`}
              </p>
            </Paper>
            <div className="text-center mt-3">
              <button type="button" className="btn-primary py-2 px-4" onClick={() => showToast(toastTypeFlag, direction)}>
                Show Toast
              </button>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default ReactToastify;
