import React from 'react';
import PropTypes from 'prop-types';
const Message = ({ msg, symbol }) => (
  <div className="row">
    <div className="col-2">{symbol}</div>
    <div className="col-10">{msg}</div>
  </div>
);
Message.propTypes = {
  msg: PropTypes.string,
  symbol: PropTypes.symbol,
};
Message.defaultProps = {
  msg: '',
  symbol: null,
};
export default Message;
