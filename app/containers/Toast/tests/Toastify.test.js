import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import ReactToastify from '../ReactToastify';
import Message from '../Message';
configure({ adapter: new Adapter() });

describe('ReactToastify', () => {
  it('should be defined', () => {
    expect(ReactToastify).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<ReactToastify />);
    expect(tree).toMatchSnapshot();
  });
  it('onChange', () => {
    const wrapper = shallow(<ReactToastify />);
    const Btn = wrapper.find('#Slide');
    Btn.simulate('change');
  });
  it('onChange', () => {
    const wrapper = shallow(<ReactToastify />);
    const Btn = wrapper.find('#Flip');
    Btn.simulate('change');
  });
  it('onChange', () => {
    const wrapper = shallow(<ReactToastify />);
    const Btn = wrapper.find('#Bounce');
    Btn.simulate('change');
  });
  it('onChange', () => {
    const wrapper = shallow(<ReactToastify />);
    const Btn = wrapper.find('#Zoom');
    Btn.simulate('change');
  });
  // it('onClick', () => {
  //   const wrapper = shallow(<ReactToastify />);
  //   const Btn = wrapper.find('button');
  //   Btn.simulate('click');
  // });
});
describe('Message', () => {
  it('should be defined', () => {
    expect(Message).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Message msg="test" symbol={<InfoIcon />} />);
    expect(tree).toMatchSnapshot();
  });
});
