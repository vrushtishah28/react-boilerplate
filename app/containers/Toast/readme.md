## Component Name

Toastify Component

## Getting Started

These code will help you in implementing toastify component easily.

### Prerequisites

Things you need to install before starting with react toastify:

```
npm install --save react-toastify

```

### How to Use

A step by step series of how you can implement toastify in react:

Firstly start by importing libraries:

```
import { toast } from 'react-toastify';

```

If you want to implement other effects in toast like bounce,flip,zoom or slide then you need to import the following:

```

import { Slide, Flip, Bounce , Zoom } from 'react-toastify';

```

## For using toastify component you'll have to implement the following code:

```
const demo = () => {
  ...
  ...

  toast("Some message",{options})
  toast.configure(); //this is mandatory for rendering in html or you can also use <ToastContainer/> instead of this

  ...
  ...
  return jsx elements
}
```

## Toast Types:

```

toast.success("Some message",{options})
toast.info("Some message",{options})
toast.error("Some message",{options})
toast.warning("Some message",{options})

```

## Toast Options:

```
  {
      position: direction,
      autoClose: boolean/seconds,
      hideProgressBar: boolean,
      closeOnClick: boolean,
      pauseOnHover: boolean,
      draggable: boolean,
      className: classname,
      transition: Zoom/Slide/Bounce/Flip,
      ...
  }

```

### For more information you can go through the following link:

- [Reference Toastify Link](https://fkhadra.github.io/react-toastify/)
