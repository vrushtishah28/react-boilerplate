/* eslint-disable func-names */
import { getAllPost } from './services';
export const autoSizeColumns = params => {
  const colIds = params.columnApi.getAllDisplayedColumns().map(col => col.getColId());
  params.columnApi.autoSizeColumns(colIds);
};
function onGridReadyFunction(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;

  params.api.sizeColumnsToFit();
  window.addEventListener('resize', function() {
    setTimeout(function() {
      params.api.sizeColumnsToFit();
    });
  });

  params.api.sizeColumnsToFit();
}
export const onGridReady = onGridReadyFunction;

export const setPost = async setData => {
  const Bigresponse = await getAllPost();
  const response = Bigresponse.data;
  setData({
    columnDefs: [
      {
        headerName: 'userId',
        field: 'userId',
        sortable: true,
        filter: true,
        resizable: true,
        suppressHorizontalScroll: true,
      },
      {
        headerName: 'id',
        field: 'id',
        sortable: true,
        filter: true,
        resizable: true,
        suppressHorizontalScroll: true,
      },
      {
        headerName: 'title',
        field: 'title',
        sortable: true,
        filter: true,
        resizable: true,
        suppressHorizontalScroll: true,
      },
      {
        headerName: 'body',
        field: 'body',
        sortable: true,
        filter: true,
        resizable: true,
        suppressHorizontalScroll: true,
      },
    ],
    rowData: response,
  });
};
