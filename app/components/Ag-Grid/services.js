import axios from 'axios';

// export const getAllPost = () => {
//   return axios
//     .get("https://jsonplaceholder.typicode.com/posts")
//     .then((response) => response.data);
// };

export const getAllPost = async () => {
  const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
  return response;
};
