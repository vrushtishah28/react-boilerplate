import React, { useState } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-community/dist/styles/ag-theme-balham-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-blue.css';
import 'ag-grid-community/dist/styles/ag-theme-bootstrap.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import 'ag-grid-community/dist/styles/ag-theme-fresh.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import PropTypes from 'prop-types';
import { onGridReady, autoSizeColumns, setPost } from './utils';
import './custom-theme.css';
function Grid({ theme, height, width, autowidth }) {
  const [data, setData] = useState({});
  setPost(setData);
  // if (data === {}) {
  //   return <p>Loading</p>;
  // }
  return (
    <div className={theme === 'custom-theme' ? 'custom-theme' : null}>
      <div
        className={theme === 'custom-theme' ? 'ag-theme-balham' : theme}
        style={{
          maxWidth: width,
          height,
        }}
      >
        <AgGridReact
          pagination
          paginationPageSize="8"
          domLayout="autoHeight"
          onFirstDataRendered={autowidth ? onGridReady : autoSizeColumns}
          columnDefs={data.columnDefs}
          rowHeight="50"
          headerHeight="50"
          rowData={data.rowData}
        />
      </div>
    </div>
  );
}
Grid.propTypes = {
  theme: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  autowidth: PropTypes.bool,
};
Grid.defaultProps = {
  theme: false,
  height: false,
  width: false,
  autowidth: false,
};
export default Grid;
