/* eslint-disable no-param-reassign */
/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-plusplus */
/* eslint-disable consistent-return */
/* eslint-disable func-names */
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { AgGridReact } from 'ag-grid-react/lib/agGridReact';
import Grid from '../Grid';
import { getAllPost } from '../services';
configure({ adapter: new Adapter() });
jest.mock('axios');

describe('Grid', () => {
  it('should be defined', () => {
    expect(Grid).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Grid />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly', () => {
    const tree = shallow(<Grid theme="custom-theme" autowidth />);
    expect(tree).toMatchSnapshot();
  });
  it('services should be called correctly', async () => {
    await expect(await getAllPost()).resolves;
  });
  it('should render correctly', () => {
    const wrapper = shallow(<Grid />);
    const agGridReact = wrapper.find(AgGridReact);
    agGridReact.simulate('gridReady');
  });
});
