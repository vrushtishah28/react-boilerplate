import React from 'react';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import PropTypes from 'prop-types';
function Cards({ data, className }) {
  const { title, description, imgUrl, link } = data || {};
  return (
    <Link to={link}>
      <Card className={`card ${className}`}>
        <CardActionArea>
          <CardMedia style={{ height: 140 }} image={imgUrl} title="Card image" />
          <CardContent>
            <Typography gutterBottom variant="h6" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {description}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Link>
  );
}
Cards.propTypes = {
  data: PropTypes.object,
  className: PropTypes.string,
};
Cards.defaultProps = {
  data: {},
  className: '',
};
export default Cards;
