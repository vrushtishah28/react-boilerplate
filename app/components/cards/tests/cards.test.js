import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Cards from '../Cards';
import CardList from '../CardList';
configure({ adapter: new Adapter() });
const demodata = [
  {
    title: 'Demo',
    name: 'Demo',
    description: 'Demo',
    link: 'TestLink',
  },
  {
    title: 'Demo',
    name: 'Demo',
    description: 'Demo',
    link: 'TestLink',
  },
];
describe('Cards', () => {
  it('should be defined', () => {
    expect(Cards).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Cards />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly', () => {
    const tree = shallow(<Cards data={demodata[0]} className="data" />);
    expect(tree).toMatchSnapshot();
  });
});
describe('CardList', () => {
  it('should be defined', () => {
    expect(CardList).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<CardList data={demodata} className="data" />);
    expect(tree).toMatchSnapshot();
  });
});
