import React from 'react';
import PropTypes from 'prop-types';
import Cards from './Cards';
import './Cards.css';

function CardList({ data, className }) {
  return (
    <div className={className}>
      {data.map(item => (
        <Cards data={item} className="mb-4" />
      ))}
    </div>
  );
}

CardList.propTypes = {
  data: PropTypes.array,
  className: PropTypes.string,
};
CardList.defaultProps = {
  data: [],
  className: '',
};
export default CardList;
