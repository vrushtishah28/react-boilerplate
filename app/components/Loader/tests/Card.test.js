import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Card from '../Card';
configure({ adapter: new Adapter() });

describe('Card', () => {
  it('should be defined', () => {
    expect(Card).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Card />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly', () => {
    const tree = shallow(<Card status />);
    expect(tree).toMatchSnapshot();
  });
});
