/* eslint-disable react/prop-types */
import React from 'react';

const Button = ({ children, className, onClick, id }) => {
  if (children) {
    return (
      <button type="button" className={className} onClick={onClick} id={id}>
        {children}
      </button>
    );
  }

  return <button type="button"> </button>;
};

export default Button;
