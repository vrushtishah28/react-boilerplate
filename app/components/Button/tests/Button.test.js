import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button from '../index';
configure({ adapter: new Adapter() });

describe('Button', () => {
  it('should be defined', () => {
    expect(Button).toBeDefined();
  });
  it('should render correctly', () => {
    const tree = shallow(<Button />);
    expect(tree).toMatchSnapshot();
  });
  it('should render correctly', () => {
    const tree = shallow(<Button>Test</Button>);
    expect(tree).toMatchSnapshot();
  });
});
